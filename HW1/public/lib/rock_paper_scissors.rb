#In a game of rock-paper-scissors, each player chooses to play Rock (R), Paper (P), or Scissors (S).
#The rules are: Rock breaks Scissors, Scissors cuts Paper, but Paper covers Rock.
#In a round of rock-paper-scissors, each player's name and strategy is encoded as an array of two elements
#[ ["Armando", "P"], ["Dave", "S"] ] # Dave would win since S > P

class RockPaperScissors

  # Exceptions this class can raise:
  class NoSuchStrategyError < StandardError ; end

  def self.winner(player1, player2)
    raise NoSuchStrategyError, "Strategy must be one of R,P,S" if !player1[1].match(/[PRS]{1}/) or !player2[1].match(/[PRS]{1}/)

    strategy = {'R' => 1,  'S' => 2 ,  'P' => 3}

    if player1[1] == player2[1]
      return player1

    elsif [2, -1].include? (strategy[player1[1]] - strategy[player2[1]])
      return player1

    elsif [-2, 1, 2].include? (strategy[player1[1]] - strategy[player2[1]])
      return player2

    end
  end

  def self.tournament_winner(tournament)

    if tournament[0][0].is_a? String
       winner(tournament[0], tournament[1])
    else
      tournament_winner( [tournament_winner(tournament[0]), tournament_winner(tournament[1])])
    end
  end

end

@rock = ['Armando','R'] ; @paper = ['Dave','P'] ; @scissors = ['Sam','S']
 tourney = [
        [
          [ ["Armando", "P"], ["Dave", "S"] ],
          [ ["Richard", "R"], ["Michael", "S"] ]
        ],
        [
          [ ["Allen", "S"], ["Omer", "P"] ],
          [ ["David E.", "R"], ["Richard X.", "P"] ]
        ]
      ]
p RockPaperScissors.tournament_winner(tourney)
#p RockPaperScissors.tournament_winner([@rock,@paper])


module FunWithStrings
  def palindrome?
    word = self.downcase.gsub(" ", "").gsub(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/, "")
    word == word.reverse
  end
  def count_words
    word = self.downcase.scan(/\w+/).inject(Hash.new 0){ |frequency, word| frequency[word]+= 1; frequency }
  end
  def anagram_groups
    # your code here
    # if it hasnt same size, it isn't an anagram
    # case insensitive
    words = self.downcase.scan(/\w+/).inject(Hash.new { |value,key| value[key] = []}) { |value,word| value[word.split(//).sort] << word; value}
  #{["a", "c", "e", "m", "r", "s"]=>["scream", "creams"], ["a", "c", "r", "s"]=>["cars", "scar"], ["f", "o", "r"]=>["for"], ["f", "o", "r", "u"]=>["four"]}
  end
end

# make all the above functions available as instance methods on Strings:

class String
  include FunWithStrings
end


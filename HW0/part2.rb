#Define a method hello(name) that takes a string representing
#a name and returns the string "Hello, " concatenated with the name.

def hello(name)
	"Hello, #{name}"
end

#Define a method starts_with_consonant?(s) that takes a string and returns
#true if it starts with a consonant and false otherwise.
#(For our purposes, a consonant is any letter other than A, E, I, O, U.)
#NOTE: be sure it works for both upper and lower case and for nonletters!

def starts_with_consonant?(s)
	/^[^aeiou$#:]/i =~ s
end

#Define a method binary_multiple_of_4?(s) that takes a string and returns true 
#if the string represents a binary number that is a multiple of 4. NOTE: be sure
#it returns false if the string is not a valid binary number!

def binary_multiple_of_4?(s)
	/^[01]*1[01]*00$/ =~ s
end



#Test exercice 1

#p hello(1231)
#p hello("Vitor")

#Test exercice 2

#p starts_with_consonant?('oo')
#p starts_with_consonant?('Oo')
#p starts_with_consonant?('foo')
#p starts_with_consonant?('Foo')
#p starts_with_consonant?('#foo')

#Test exercice 3
#p binary_multiple_of_4?('0000')
#p binary_multiple_of_4?('1100')
#p binary_multiple_of_4?('string') #NOTE: be sure it returns false if the string is not a valid binary number!


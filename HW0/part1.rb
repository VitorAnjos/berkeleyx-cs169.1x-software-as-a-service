#Define a method sum which takes an array of integers as an argument and returns the #sum of its elements. For an empty array it should return zero.

def sum(array)
  sum = 0
  array.each do |element|
    sum += element
  end
  return sum
end


#Define a method max_2_sum which takes an array of integers as an argument and returns #the sum of its two largest elements. For an empty array it should return zero. For an #array with just one element, it should return that element.

def max_2_sum(array)
  sum(array.sort.pop(2))
end

#Define a method sum_to_n? which takes an array of integers and an additional integer, #n, as arguments and returns true if any two elements in the array of integers sum to #n. An empty array should sum to zero by definition.

def sum_to_n?(array,n)
  return false if ((array.length == 0) && (n!=0))
  return 0 if array.length == 0
   while array.length > 0
		last = array.pop # pops the last value of the array
		array.each do |number|
			return true if number + last == n # returns if the sum of the current index with the last value equals n
		end
	end
end




#tests for exercice 1

#puts "sum([]) == 0"
#puts sum([]) == 0

#puts "sum([1,2]) == 3"
#puts sum([1,2]) == 3

#puts "sum([1,2,3,4,5,6,7,8,9,10]) == 55"
#puts sum([1,2,3,4,5,6,7,8,9,10]) == 55


#tests for exercice 2


#puts "max_2_sum([]) == 0"
#puts max_2_sum([]) == 0

#puts "max_2_sum([3]) == 3"
#puts max_2_sum([3]) == 3

#puts "max_2_sum([1,2,3,4]) == 7"
#puts max_2_sum([1,2,3,4]) == 7

#tests for exercice 3

#puts "sum_to_n?([1,2,3], 5) == true" 
#puts sum_to_n?([1,2,3], 5) == true

#puts "sum_to_n?([1,2,3,4,5], 10) == false"
#puts sum_to_n?([1,2,3,4,5], 10) == false

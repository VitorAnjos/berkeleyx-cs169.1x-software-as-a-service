#HW0 PART 3
#Define a class BookInStock which represents a book with an isbn number, isbn, and price of the book as a floating-point number,
#price, as attributes. The constructor should accept the ISBN number (a string) as the first argument and price as second argument,
#and should raise ArgumentError (one of Ruby's built-in exception types) if the ISBN number is the empty string or if the price is
#less than or equal to zero.

#Include the proper getters and setters for these attributes. Include a method price_as_string that displays the price of the book with
#a leading dollar sign and trailing zeros, that is, a price of 20 should display as "$20.00" and a price of 33.8 should display as "$33.80".

#Please put the class in a single file and upload it using the upload form below:

class BookInStock

	def initialize(isbn, price)
		self.isbn = isbn
		self.price = price
	end

	def isbn=(isbn)
		
		if isbn == nil or isbn.size == 0
			raise ArgumentError.new('ISBN number cannot be empty.')			
		end

		@isbn = isbn
	end

	def price=(price)
		
		if price == nil or price <= 0
			raise ArgumentError.new('Price cannot be less than or equal to zero.')
		end

		@price = price
	end

	def isbn
		@isbn
	end

	def price
		@price
	end

	def price_as_string
		sprintf("$%.2f", price)
	end
end

#gameOfThrones = BookInStock.new('09213212',33.8)
#p gameOfThrones.price_as_string

require 'debugger'              # optional, may be helpful
require 'open-uri'              # allows open('http://...') to return body
require 'cgi'                   # for escaping URIs
require 'nokogiri'              # XML parser
require 'active_model'          # for validations

class OracleOfBacon

  class InvalidError < RuntimeError ; end
  class NetworkError < RuntimeError ; end
  class InvalidKeyError < RuntimeError ; end

  attr_accessor :from, :to
  attr_reader :api_key, :response, :uri

  include ActiveModel::Validations
  validates_presence_of :from
  validates_presence_of :to
  validates_presence_of :api_key
  validate :from_does_not_equal_to


  def from_does_not_equal_to
    if self.from == self.to
      errors.add(:to,  "From cannot be the same as To ")
    end
  end

  def initialize(api_key='')
    @api_key = api_key
    self.to = "Kevin Bacon"
    self.from = "Kevin Bacon"
  end

  def find_connections
    make_uri_from_arguments
    begin
      xml = URI.parse(uri).read
    rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError,
      Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError,
      Net::ProtocolError => e
      raise OracleOfBacon::NetworkError
      # convert all of these into a generic OracleOfBacon::NetworkError,
      #  but keep the original error message
      # your code here
    end
    # your code here: create the OracleOfBacon::Response object
    OracleOfBacon::Response.new(xml)
  end

  def make_uri_from_arguments
    uri_aux= "http://oracleofbacon.org/cgi-bin/xml?p=#{@api_key}&b=#{@to}&a=#{@from}".gsub(' ', '+')
    @uri = URI::escape(uri_aux)
  end

  class Response
    attr_reader :type, :data
    # create a Response object from a string of XML markup.
    def initialize(xml)
      @doc = Nokogiri::XML(xml)
      parse_response

    end

    private

    def parse_response
      if ! @doc.xpath('/error').empty?
        parse_error_response

      elsif !@doc.xpath('/link').empty?
        parse_graph_response

      elsif !@doc.xpath('/spellcheck').empty?
        parse_spellcheck_response

      else
        parse_unknown
      end
    end

    def parse_error_response
      @type = :error
      @data = 'Unauthorized access'
    end

    def parse_graph_response
      @type = :graph

      actors =[]
      movies = []

      @doc.xpath("//actor").each{ |actor| actors.push(actor.text)}
      @doc.xpath("//movie").each{ |movie| movies.push(movie.text)}

      @data = actors.zip(movies).flatten.compact
    end

    def parse_spellcheck_response
      @type = :spellcheck
      @data = []
      @doc.xpath('//match').each { |match| @data.push(match.text)}

    end

    def parse_unknown
      @type = :unknown
      @data = 'unknown response'
    end
  end
end

#oob = OracleOfBacon.new('fake_key')
      #oob.from = '3%2 "a' ; oob.to = 'George Clooney'
  #    oob.make_uri_from_arguments

